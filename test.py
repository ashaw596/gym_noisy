import gym
import gym_noisy
import numpy as np

# env = gym.make('Banana-v0')
# env = gym.make('Hopper-v1')
env = gym.make('Hopper-noisy-v1')
action = env.action_space.sample()
env.seed(0)
print(env.action_space.sample())
initial_start = env.reset()
initial_all_obs = []

env.seed(0)
for t in range(900):
    initial_next_obs, reward, done, info = env.step(action)
    initial_all_obs.append(initial_next_obs)
initial_all_obs = np.array(initial_all_obs)
# print('start', start, 'next', next)


for i in range(100):

    env.seed(0)
    start = env.reset()
    env.seed(i)
    all_next_obs = []
    for t in range(900):
        next_obs, reward, done, info = env.step(action)
        all_next_obs.append(next_obs)
    all_next_obs = np.array(all_next_obs)

    print(i, 'start same:', np.array_equal(start,initial_start), 'next_same', np.array_equal(all_next_obs, initial_all_obs))
    # print(next_obs)
    # print(initial_next_obs)

    # print(np.sum(np.array(next)-np.array(initial_next)))

# print(env)