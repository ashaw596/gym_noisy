from gym.envs.registration import register

register(
    id='InvertedPendulum-noisy-v1',
    entry_point='gym_noisy.envs.noisy_envs:InvertedPendulumEnvNoisy',
    max_episode_steps=1000,
    reward_threshold=950.0,
)

register(
    id='InvertedDoublePendulum-noisy-v1',
    entry_point='gym_noisy.envs.noisy_envs:InvertedDoublePendulumEnvNoisy',
    max_episode_steps=1000,
    reward_threshold=9100.0,
)

register(
    id='HalfCheetah-noisy-v1',
    entry_point='gym_noisy.envs.noisy_envs:HalfCheetahEnvNoisy',
    max_episode_steps=1000,
    reward_threshold=4800.0,
)

register(
    id='Hopper-noisy-v1',
    entry_point='gym_noisy.envs.noisy_envs:HopperEnvNoisy',
    max_episode_steps=1000,
    reward_threshold=3800.0,
)

register(
    id='Swimmer-noisy-v1',
    entry_point='gym_noisy.envs.noisy_envs:SwimmerEnvNoisy',
    max_episode_steps=1000,
    reward_threshold=360.0,
)

register(
    id='Walker2d-noisy-v1',
    max_episode_steps=1000,
    entry_point='gym_noisy.envs.noisy_envs:Walker2dEnvNoisy',
)

register(
    id='Ant-noisy-v1',
    entry_point='gym_noisy.envs.noisy_envs:AntEnvNoisy',
    max_episode_steps=1000,
    reward_threshold=6000.0,
)

register(
    id='Humanoid-noisy-v1',
    entry_point='gym_noisy.envs.noisy_envs:HumanoidEnvNoisy',
    max_episode_steps=1000,
)

register(
    id='HumanoidStandup-noisy-v1',
    entry_point='gym_noisy.envs.noisy_envs:HumanoidStandupEnvNoisy',
    max_episode_steps=1000,
)