from gym.envs.mujoco import *
import numpy as np

def noisy(cls):
    def __init__(self):
        self.action_noise_std = 0
        self.obs_noise_std = 0
        self.seed()
        super(cls, self).__init__()
        self.obs_noise_std = 0.05

    def _step(self, a):
        if self.action_noise_std:
            a_noisy = np.clip(self.np_random.normal(a, self.action_noise_std), a_min=self.action_space.low, a_max=self.action_space.high)
        else:
            a_noisy = a
        ob, reward, done, _ = super(cls,self)._step(a_noisy)

        if self.obs_noise_std:
            ob_noisy = np.clip(self.np_random.normal(ob, self.obs_noise_std), a_min=self.observation_space.low, a_max=self.observation_space.high)
        else:
            ob_noisy = ob
        return ob_noisy, reward, done, {}

    cls.__init__ = __init__
    cls._step = _step
    return cls

@noisy
class AntEnvNoisy(AntEnv):
    pass
@noisy
class HalfCheetahEnvNoisy(HalfCheetahEnv):
    pass
@noisy
class HopperEnvNoisy(HopperEnv):
    pass
@noisy
class Walker2dEnvNoisy(Walker2dEnv):
    pass
@noisy
class HumanoidEnvNoisy(HumanoidEnv):
    pass
@noisy
class InvertedPendulumEnvNoisy(InvertedPendulumEnv):
    pass
@noisy
class InvertedDoublePendulumEnvNoisy(InvertedDoublePendulumEnv):
    pass
@noisy
class ReacherEnvNoisy(ReacherEnv):
    pass
@noisy
class SwimmerEnvNoisy(SwimmerEnv):
    pass
@noisy
class HumanoidStandupEnvNoisy(HumanoidStandupEnv):
    pass
@noisy
class PusherEnvNoisy(PusherEnv):
    pass
@noisy
class ThrowerEnvNoisy(ThrowerEnv):
    pass
@noisy
class StrikerEnvNoisy(StrikerEnv):
    pass